import os
import sys

from netorca_sdk.consumer import ConsumerSubmission

if __name__ == "__main__":
    NETORCA_VALIDATE_ONLY = os.environ.get("NETORCA_VALIDATE_ONLY", "True")
    REPOSITORY_URL = os.environ.get("REPOSITORY_URL", "./")

    NETORCA_API_KEY = os.environ.get("NETORCA_API_KEY")

    if not NETORCA_API_KEY:
        print("NETORCA_API_KEY not provided")
        sys.exit(1)

    consumer = ConsumerSubmission(netorca_api_key=NETORCA_API_KEY)
    consumer.load_from_repository(REPOSITORY_URL)

    if NETORCA_VALIDATE_ONLY == "True":
        response = consumer.validate(pretty_print=True)
    elif NETORCA_VALIDATE_ONLY == "False":
        response = consumer.submit()
    else:
        print("Wrong NETORCA_VALIDATE_ONLY variable, available values: 'False', 'True'")
        sys.exit(1)

    if response[0]:
        if NETORCA_VALIDATE_ONLY == "True":
            print(f"Validation successful.")
        elif NETORCA_VALIDATE_ONLY == "False":
            print(f"Submission successful.")
    else:
        sys.exit(1)
